package cn.com.eado.cardview1215;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by lcy on 2015/12/15.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.NewsViewHolder>{
    private List<News> newses;
    private Context context;

    public MyAdapter(List<News> newses, Context context) {
        this.newses = newses;
        this.context = context;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=View.inflate(context,R.layout.item,null);
        NewsViewHolder vh=new NewsViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        holder.name.setText(newses.get(position).getTitle());
        holder.desc.setText(newses.get(position).getDesc());
        holder.image.setImageResource(newses.get(position).getPhotoId());
    }

    @Override
    public int getItemCount() {
        return newses.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        @InjectView(R.id.name)TextView name;
        @InjectView(R.id.desc)TextView desc;
        @InjectView(R.id.image)ImageView image;


        public NewsViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.inject(this,itemView);
            //设置TextView背景为半透明
            name.setBackgroundColor(Color.argb(20, 0, 0, 0));
        }
    }
}
