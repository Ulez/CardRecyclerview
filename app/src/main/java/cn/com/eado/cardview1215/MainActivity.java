package cn.com.eado.cardview1215;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.recyclerview)RecyclerView mRecyclerview;
//    你想要控制其显示的方式，请通过布局管理器LayoutManager
//    你想要控制Item间的间隔（可绘制），请通过ItemDecoration
//    你想要控制Item增删的动画，请通过ItemAnimator
//    你想要控制点击、长按事件，请自己写（擦，这点尼玛。）
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayoutManager layoutManager=new LinearLayoutManager(this);

        ButterKnife.inject(this);
        List<News> newsList=new ArrayList<>();
        newsList =new ArrayList<>();

        String url="";
        newsList.add(new News(getString(R.string.news_one_title), getString(R.string.news_one_desc), R.drawable.bg_one));
        newsList.add(new News(getString(R.string.news_two_title), getString(R.string.news_two_desc), R.drawable.bg_two));
        newsList.add(new News(getString(R.string.news_three_title), getString(R.string.news_three_desc), R.drawable.bg_three));
        newsList.add(new News(getString(R.string.news_four_title), getString(R.string.news_four_desc),R.drawable.bg_four));
        newsList.add(new News(getString(R.string.news_five_title), getString(R.string.news_five_desc),R.drawable.bg_five));

        MyAdapter adapter=new MyAdapter(newsList,this);

        mRecyclerview.setHasFixedSize(true);
        mRecyclerview.setLayoutManager(layoutManager);
        mRecyclerview.setAdapter(adapter);

    }
}
